(function() {
  function multipart_init() {
    Drupal = Drupal || {};
    return Drupal.multipart = {
      replace: function(region) {
        var dom = jQuery('#multipart-replacement-' + region).children(),
            target = jQuery('#multipart-placeholder-' + region).parent();
        if (dom.length === 0 || target.length === 0) {
          return;
        }
        target.empty();
        dom.appendTo(target);
        Drupal.attachBehaviors && Drupal.settings && Drupal.attachBehaviors(target, Drupal.settings);
      },
      iterate: function() {
        var placeholders = jQuery('.multipart-placeholder');
        if (placeholders.length > 0) {
          setTimeout(Drupal.multipart.iterate(), 100);
          placeholders.each(function() {
            var region = jQuery(this).attr('id').substring(22);
            Drupal.multipart.replace(region);
          })
        }
      }
    }
  }

  multipart_init().iterate();

  jQuery(multipart_init)
  jQuery(window).load(function() {
    var targets = jQuery('.region > .multipart-placeholder').parent();
    jQuery('.multipart-placeholder').remove();
    targets.each(function() {
      if (jQuery(this).children().length == 0) {
        jQuery(this).remove();
      }
    })
  })
})()