<?php

/**
 * Implements hook_menu().
 */
function multipart_menu() {
	return array(
		'admin/structure/multipart/%' => array(
			'title' => t('Multipart Regions'),
			'description' => t('Manage the importance of page regions for multi-part delivery'),
			'page callback' => 'drupal_get_form',
			'page arguments' => array('multipart_admin', 3),
			'access arguments' => array('administer site configuration')
		),
	);
}

/**
 * Implements hook_system_themes_page_alter
 */
function multipart_system_themes_page_alter(&$info) {
	foreach ($info['enabled'] as $i => $theme) {
		$theme->operations[] = array(
			'title' => t('Multipart Settings'),
			'href' => 'admin/structure/multipart/' . $theme->name,
			'attributes' => array(
				'title' => 'Manage multipart region weights for the ' . $theme->info['name'] . ' theme'
			),
		);
	}
}


function multipart_admin($form, &$form_state, $theme) {
	# get regions & settings.
	$form_state['theme'] = $theme;
	$regions = system_region_list($theme);
	$settings = _multipart_settings(false, $theme);

	$header = array('Region', 'Group');
	$rows = array();

	foreach ($regions as $slug => $title) {
		$setting = isset($settings[$slug]) ? $settings[$slug] : 'init';
		$rows[] = array(
			$title,
			array('data' => array(
				'#name' => 'regions[' . $slug . ']',
				'#type' => 'select',
				'#options' => array(
					'init' => t('Initial page delivery - exactly how Drupal does it'),
					'after' => t('Sent afterwards by multipart - does not require a secondary HTTP request'),
					'ajax' => t('Loaded in a separate AJAX request - requires a secondary HTTP request')
				),
				'#value' => $setting
			))
		);
	}

	$form['table'] = array(
		'#theme' => 'table',
		'#header' => $header,
		'#rows' => $rows
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save settings',
	);
	return $form;
}

function multipart_admin_submit($form, $form_state) {
	$values = $form_state['input'];
	$theme = $form_state['theme'];
	$settings = _multipart_settings(false);
	$settings[$theme] = $values['regions'];

	_multipart_settings($settings);
	drupal_set_message('Multipart settings updated');
}